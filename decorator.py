from functools import wraps


def swap_case_decorator(func):
    @wraps(func)
    def swap():
        result = func().swapcase()
        if isinstance(result, str):
            raise TypeError()
        return result
    return swap

@swap_case_decorator
def many_cases_hello() -> str:
    return "hElLo"
