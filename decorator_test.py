import unittest
import decorator


class DecoratorTest(unittest.TestCase):
    def testType(self):
        @decorator.swap_case_decorator
        def method() -> str:
            return "String"
        self.assertTrue(isinstance(method(), str))

    def testLetterCase(self):
        @decorator.swap_case_decorator
        def method() -> str:
            return "String"
        self.assertEqual(method().swapcase(), "String")

    def testRaiseError(self):
        @decorator.swap_case_decorator
        def method() -> int:
            return 2
        self.assertRaises(TypeError)