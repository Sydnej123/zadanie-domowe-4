import io
import sys

from contextlib import redirect_stdout

import decorator_with_arguments
import unittest


class KnownValues(unittest.TestCase):
    known_values = ('seconds',
                    'minutes',
                    'hours',
                    'days')

    random_values = ('kkbjia',
                     'dasd',
                     'h000ou')

    def testTimeUnits(self):
        for time_unit in self.known_values:
            with io.StringIO() as buf, redirect_stdout(buf):
                @decorator_with_arguments.execution_time(time_unit=time_unit)
                def function():
                    pass
                function()
                output = buf.getvalue()
            self.assertRegex(expected_regex='.* {}.'.format(time_unit), text=output, msg=None)

    def testNotTimeUnits(self):
        for time_unit in self.random_values:
            with io.StringIO() as buf, redirect_stdout(buf):
                @decorator_with_arguments.execution_time(time_unit=time_unit)
                def function():
                    pass

                function()
                output = buf.getvalue()
            self.assertRegex(text=output, expected_regex="Unknown time unit")
