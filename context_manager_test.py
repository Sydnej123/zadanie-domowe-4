import io
import unittest
from contextlib import redirect_stdout

import context_manager


class ContextTest(unittest.TestCase):
    testCases = ("abecadlo", "notatka druga", "notatka trzecia")

    def testAppend(self):
        with context_manager.Notepad("Tytul") as notepad:
            notepad.append("abecadlo")
            self.assertEqual(notepad.pop(0), "abecadlo")

    def testNotes(self):
        with io.StringIO() as buf, redirect_stdout(buf):
            with context_manager.Notepad("name") as notepad:
                notepad.append("abecadlo")
                notepad.append("notatka druga")
                notepad.append("notatka trzecia")
            output = buf.getvalue()
        for case in self.testCases:
            self.assertRegex(text=output, expected_regex=".*{}.*".format(case), msg=None)
