class Notepad(object):
    def __init__(self, notes_name):
        self.notes = list()
        self.notes_name = notes_name

    def __enter__(self):
        return self.notes

    def __exit__(self, type, value, traceback):
        print('{}:'.format(self.notes_name))
        for i, note in enumerate(self.notes):
            print("{}. {}".format(i, note))
