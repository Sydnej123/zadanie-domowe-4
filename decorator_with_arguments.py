from functools import wraps
import time


def execution_time(time_unit='seconds'):
    def execution_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            time_before_execution = time.time()
            func(*args, **kwargs)
            execution_duration = time.time() - time_before_execution
            if time_unit.upper() == 'SECONDS':
                print("Function named {} execution time is: {} seconds.".format(func.__name__, execution_duration))
            elif time_unit.upper() == 'MINUTES':
                print("Function named {} execution time is: {} minutes/".format(func.__name__, execution_duration / 60))
            elif time_unit.upper() == 'HOURS':
                print("Function named {} execution time is: {} hours/".format(func.__name__, execution_duration / 3600))
            elif time_unit.upper() == 'DAYS':
                print("Function named {} execution time is: {} days.".format(func.__name__,
                                                                            execution_duration / (3600 * 24)))

            else:
                print("Unknown time unit")
        return wrapper
    return execution_decorator


@execution_time(time_unit='seconds')
def time_function() -> None:
    for i in range(0, 100):
        print(i)
